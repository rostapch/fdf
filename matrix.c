/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   matrix.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <rostapch@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/16 13:07:19 by rostapch          #+#    #+#             */
/*   Updated: 2017/04/03 18:18:31 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

double	**cabinet(void)
{
	double	**cabinet;

	cabinet = (double **)malloc(sizeof(double *) * 4);
	cabinet[0] = (double *)malloc(sizeof(double) * 4);
	cabinet[1] = (double *)malloc(sizeof(double) * 4);
	cabinet[2] = (double *)malloc(sizeof(double) * 4);
	cabinet[3] = (double *)malloc(sizeof(double) * 4);
	cabinet[0][0] = 1;
	cabinet[0][1] = 0;
	cabinet[0][2] = 0.5;
	cabinet[0][3] = 0;
	cabinet[1][0] = 0;
	cabinet[1][1] = 1;
	cabinet[1][2] = 0.5;
	cabinet[1][3] = 0;
	cabinet[2][0] = 0;
	cabinet[2][1] = 0;
	cabinet[2][2] = 0;
	cabinet[2][3] = 0;
	cabinet[3][0] = 0;
	cabinet[3][1] = 0;
	cabinet[3][2] = 0;
	cabinet[3][3] = 1;
	return (cabinet);
}

double	**izometric(void)
{
	double	**izometric;

	izometric = (double **)malloc(sizeof(double *) * 4);
	izometric[0] = (double *)malloc(sizeof(double) * 4);
	izometric[1] = (double *)malloc(sizeof(double) * 4);
	izometric[2] = (double *)malloc(sizeof(double) * 4);
	izometric[3] = (double *)malloc(sizeof(double) * 4);
	izometric[0][0] = 0.707;
	izometric[0][1] = -0.707;
	izometric[0][2] = 0;
	izometric[0][3] = 0;
	izometric[1][0] = 0.409;
	izometric[1][1] = 0.409;
	izometric[1][2] = -0.816;
	izometric[1][3] = 0;
	izometric[2][0] = 0.577;
	izometric[2][1] = 0.577;
	izometric[2][2] = 0.578;
	izometric[2][3] = 0;
	izometric[3][0] = 0;
	izometric[3][1] = 0;
	izometric[3][2] = 0;
	izometric[3][3] = 1;
	return (izometric);
}

double	**matrix(int mtrx)
{
	if (mtrx == 0)
		return (cabinet());
	else
		return (izometric());
}
