# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: rostapch <rostapch@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/04/03 17:56:15 by rostapch          #+#    #+#              #
#    Updated: 2017/04/06 17:37:29 by rostapch         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = gcc
CFLAGS = -Wall -Wextra -Werror

NAME = fdf

FRAMEFLAGS = -framework OpenGL -framework AppKit

INCLUDE_LIB = -L libft -l ft -L minilibx_macos_2016 -l mlx

HEADERS =  -I . -I libft

SRC_C =	array_operations.c	conv_3d_2d.c	data.c	drawing.c	error_exit.c\
		readfile.c			matrix.c		turn.c	main.c

SRC_O =	array_operations.o	conv_3d_2d.o	data.o	drawing.o	error_exit.o\
		readfile.o			matrix.o		turn.o	main.o

OBJ = $(SRC_C:.c=.o)

all: libx lib $(OBJ)
	$(CC) $(CFLAGS) -o $(NAME) $(SRC_O) $(FRAMEFLAGS) $(INCLUDE_LIB) $(HEADERS)

%o: %c fdf.h
	$(CC) $(CFLAGS) -c $(HEADERS)  -o $@ $<

lib:
	make -C libft -f Makefile

lib_re:
	make re -C libft -f Makefile

lib_clean:
	make clean -C libft -f Makefile

lib_fclean:
	make fclean -C libft -f Makefile



libx:
	make -C minilibx_macos_2016 -f Makefile

libx_clean:
	make clean -C minilibx_macos_2016 -f Makefile

libx_re:
	make re -C minilibx_macos_2016 -f Makefile



clean_fdf:
	/bin/rm -f $(SRC_O)

fclean_fdf: clean_fdf
	/bin/rm -f $(NAME)

clean: lib_clean libx_clean
	/bin/rm -f $(SRC_O)



fclean: lib_fclean clean
	/bin/rm -f $(NAME)

re: fclean_fdf lib_re libx_re $(OBJ)
	$(CC) $(CFLAGS) -o $(NAME) $(SRC_O) $(FRAMEFLAGS) $(INCLUDE_LIB) $(HEADERS)