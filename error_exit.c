/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error_exit.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <rostapch@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/16 13:38:53 by rostapch          #+#    #+#             */
/*   Updated: 2017/04/09 17:40:08 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	error_exit(int err)
{
	if (err == -1)
		perror("Error opening file");
	else if (err == 2)
		write(1, "Error: not correct projection", 29);
	else if (err == 3)
	{
		write(1, "Error: not enough arguments\n./fdf [file] [projection] ", 54);
		write(1, "(size xy) (size z)\n\n", 20);
		write(1, "projections:\n", 13);
		write(1, "\t[iso] - isometric\n", 19);
		write(1, "\t[cabinet] - cabinet\n", 21);
	}
	else if (err == 4)
		write(1, "Error: empty file\n", 18);
	else if (err == 5)
		write(1, "Error: not correct input\n", 25);
	exit(1);
}
