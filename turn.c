/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   turn.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <rostapch@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/03 18:05:44 by rostapch          #+#    #+#             */
/*   Updated: 2017/04/03 18:20:21 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

double	**turn_oz(int turn)
{
	double	**oz;

	oz = (double **)malloc(sizeof(double *) * 4);
	oz[0] = (double *)malloc(sizeof(double) * 4);
	oz[1] = (double *)malloc(sizeof(double) * 4);
	oz[2] = (double *)malloc(sizeof(double) * 4);
	oz[3] = (double *)malloc(sizeof(double) * 4);
	oz[0][0] = cos(turn * 0.0174532925);
	oz[0][1] = -sin(turn * 0.0174532925);
	oz[0][2] = 0;
	oz[0][3] = 0;
	oz[1][0] = sin(turn * 0.0174532925);
	oz[1][1] = cos(turn * 0.0174532925);
	oz[1][2] = 0;
	oz[1][3] = 0;
	oz[2][0] = 0;
	oz[2][1] = 0;
	oz[2][2] = 2;
	oz[2][3] = 0;
	oz[3][0] = 0;
	oz[3][1] = 0;
	oz[3][2] = 0;
	oz[3][3] = 1;
	return (oz);
}

double	**turn_axes(int dg)
{
	return (turn_oz(dg));
}
