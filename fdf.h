/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <rostapch@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/15 14:22:22 by rostapch          #+#    #+#             */
/*   Updated: 2017/07/04 15:27:55 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H

# include "minilibx_macos_2016/mlx.h"
# include "math.h"
# include "libft.h"
# include "get_next_line.h"
# include <unistd.h>
# include <stdio.h>
# include <stdlib.h>
# include <fcntl.h>

# define WINDOW_WIDTH 1000
# define WINDOW_HEIGHT 1000

typedef struct	s_wndw
{
	void		*mlx;
	void		*wnd;
}				t_window;

typedef struct	s_dt
{
	double		**z;
	int			xy[2];
	int			projection;
}				t_data;

double			**matrix(int mtrx);
void			draw_line(double *xy1, double *xy2, void **mw, double *mm_z);
double			**read_file(char *file, char *projection, int xy[2]);
void			tab_3d_2d(double **tab, int *xy, int proj_turn[2], int size[2]);
void			error_exit(int err);
void			get_window(int init, void **mlx, void **wnd);
void			draw_field(double ***tab, int x, int y);
int				min_of_2(double a, double b);
double			arr_max(double ***arr, int id, int x, int y);
double			arr_min(double ***arr, int id, int x, int y);
void			get_window(int init, void **mlx, void **wnd);
void			get_size(int size[2], int init);
int				get_projection(char *projection, int init);
double			**get_tab(double **z, int xy[2], int projection, int init);
double			**turn_axes(int dg);
#endif
